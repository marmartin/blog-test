Command line instructions

You can also upload existing files from your computer using the instructions below.
Git global setup

git config --global user.name "Marta Martin"
git config --global user.email "mrodriguezmarti@gmail.com"

Create a new repository

git clone git@gitlab.com:marmartin/blog-test.git
cd blog-test
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder

cd existing_folder
git init
git remote add origin git@gitlab.com:marmartin/blog-test.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:marmartin/blog-test.git
git push -u origin --all
git push -u origin --tags


hacer commit
se hace una modificación en el archivo README.md
git add README.md
commit -m "-m para añadir un comentario al commit que se va a hacer"


